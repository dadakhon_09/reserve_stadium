# Generated by Django 4.2.2 on 2023-06-17 09:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='stadium_owner',
        ),
        migrations.AddField(
            model_name='stadium',
            name='owner',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to='app.userprofile'),
            preserve_default=False,
        ),
    ]
