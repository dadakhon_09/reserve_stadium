from rest_framework import serializers
from app.models import Order
from app.stadium.serializers import StadiumSerializer


class OrderSerializer(serializers.ModelSerializer):
    stadium = StadiumSerializer()

    class Meta:
        model = Order
        fields = ('id', 'client_name', 'client_phone_number', 'status', 'start_time', 'end_time', 'price', 'created_at', 'stadium')
