from django.contrib.auth import authenticate
from django.utils.timezone import now
from rest_framework.authtoken.admin import User
from rest_framework.authtoken.models import Token
from rest_framework.decorators import permission_classes
from rest_framework.generics import ListAPIView, RetrieveUpdateAPIView, RetrieveDestroyAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from app.models import UserProfile
from .serializers import UserProfileSerializer


class UserProfileListAPIView(ListAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    def get_queryset(self):
        return UserProfile.objects.filter(deleted_at=None)


class UserProfileRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer


class UserProfileRetrieveDestroyAPIView(RetrieveDestroyAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    def get_queryset(self):
        return UserProfile.objects.filter(deleted_at=None)

    def destroy(self, request, *args, **kwargs):
        UserProfile.objects.filter(user_id=kwargs.get('pk')).update(deleted_at=now())
        return Response('User is deleted')


class UserCreate(APIView):
    """
    {"username": "test_owner", "password":"123", "role":"stadium_owner"}
    """

    def post(self, request):
        data = request.data
        username = data['username']
        password = data['password']
        role = data['role']

        user_check = User.objects.filter(username=username)
        if not user_check:
            new_user = User.objects.create_user(username=username, password=password)
            # token, _ = Token.objects.get_or_create(user=new_user)
            new_user.userprofile.role = role
            new_user.userprofile.save()
            new_user.save()
            return Response("User is created")
        else:
            return Response("We have already the same username")
#
#
# # @permission_classes(AllowAny)
# class UserLogin(APIView):
#     """
#     {"username": "test_owner", "password":"123"}
#     """
#
#     def post(self, request):
#         data = request.data
#         username = data['username']
#         password = data['password']
#         if username is None or password is None:
#             return Response({'error': 'Please provide both username and password!'})
#
#         user = authenticate(username=username, password=password)
#         if not user:
#             return Response({'error': 'Invalid credentials!'})
#         token, _ = Token.objects.get_or_create(user=user)
#         profile = UserProfile.objects.get(user=user)
#         return Response({'token': token.key,
#                          'user_id': user.id,
#                          'username': user.username,
#                          'role': profile.get_role_display()})
#
#
# class UserLogout(APIView):
#     def get(self, request):
#         if request.user:
#             print(request.user)
#             request.user.auth_token.delete()
#         else:
#             Response("Please login first")
#         return Response("Successfully logged out")
