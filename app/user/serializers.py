from rest_framework.authtoken.admin import User
from rest_framework.serializers import ModelSerializer
from app.models import UserProfile


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'is_active')


class UserProfileSerializer(ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = UserProfile
        fields = ('role', 'user')
