from django.contrib.auth.models import AbstractUser, User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class UserProfile(models.Model):
    ROLE_CHOICES = (
        ('admin', 'Admin'),
        ('user', 'User'),
        ('stadium_owner', 'StadiumOwner'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    role = models.CharField(max_length=20, choices=ROLE_CHOICES, default='user')
    deleted_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'user_profiles'

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        up = UserProfile(user=instance)
        up.save()


class Stadium(models.Model):
    owner = models.ForeignKey(UserProfile, on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=13)
    price = models.FloatField()
    lat = models.FloatField()
    long = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'stadiums'

    def __str__(self):
        return self.name


class File(models.Model):
    object_id = models.IntegerField()
    file_name = models.CharField(max_length=255, unique=True)
    shown_name = models.CharField(max_length=255, default=file_name)
    created_at = models.DateTimeField(auto_now_add=True)
    deleted_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'files'

    def __str__(self):
        return self.shown_name


class Order(models.Model):
    ORDER_STATUSES = (
        ('in_process', 'in_process'),
        ('reserved', 'reserved'),
        ('canceled', 'canceled'),
        ('finished', 'finished')
    )

    stadium = models.ForeignKey(Stadium, on_delete=models.DO_NOTHING)
    client_name = models.CharField(max_length=200, null=True, blank=True)
    client_phone_number = models.CharField(max_length=13, null=True, blank=True)
    price = models.FloatField(default=0)
    status = models.CharField(choices=ORDER_STATUSES, max_length=50, null=False, blank=False)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'orders'

    def __str__(self):
        return f"{self.client_name}'s order for stadium {self.stadium.name}"
