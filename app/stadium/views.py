from django.utils.timezone import now
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveUpdateAPIView, RetrieveDestroyAPIView
from rest_framework.response import Response

from app.models import Stadium
from .serializers import StadiumSerializer


class StadiumListAPIView(ListAPIView):
    queryset = Stadium.objects.all()
    serializer_class = StadiumSerializer

    def get_queryset(self):
        return Stadium.objects.filter(deleted_at=None)


class StadiumCreateAPIView(CreateAPIView):
    queryset = Stadium.objects.all()
    serializer_class = StadiumSerializer


class StadiumRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Stadium.objects.all()
    serializer_class = StadiumSerializer

    def get_queryset(self):
        return Stadium.objects.filter(deleted_at=None, )


class StadiumRetrieveDestroyAPIView(RetrieveDestroyAPIView):
    queryset = Stadium.objects.all()
    serializer_class = StadiumSerializer

    def destroy(self, request, *args, **kwargs):
        Stadium.objects.filter(id=kwargs.get('pk')).update(deleted_at=now())
        return Response('Stadium is deleted')
