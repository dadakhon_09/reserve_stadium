from rest_framework import serializers
from app.models import Stadium, File
from app.user.serializers import UserProfileSerializer


class StadiumSerializer(serializers.ModelSerializer):
    owner = UserProfileSerializer()

    class Meta:
        model = Stadium
        fields = ('id', 'name', 'address', 'phone_number', 'price', 'lat', 'long', 'created_at', 'owner')


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = '__all__'
