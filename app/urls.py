from django.urls import path

from app.order.views import OrderListAPIView, OrderCreateAPIView, OrderRetrieveUpdateAPIView, \
    OrderRetrieveDestroyAPIView
from app.stadium.views import StadiumListAPIView, StadiumCreateAPIView, StadiumRetrieveUpdateAPIView, \
    StadiumRetrieveDestroyAPIView
from app.user.views import UserProfileListAPIView, UserProfileRetrieveUpdateAPIView, UserProfileRetrieveDestroyAPIView, UserCreate
    # UserLogin, UserLogout,

urlpatterns = [
    # User URLs
    # path('users/login/', UserLogin.as_view(), name='user-login'),
    # path('users/logout/', UserLogout.as_view(), name='user-logout'),
    path('users/create/', UserCreate.as_view(), name='user-create'),
    path('users/', UserProfileListAPIView.as_view(), name='user-list'),
    path('users/<int:pk>/', UserProfileRetrieveUpdateAPIView.as_view(), name='user-detail'),
    path('users/<int:pk>/delete/', UserProfileRetrieveDestroyAPIView.as_view(), name='user-delete'),

    # Stadium URLs
    path('stadiums/', StadiumListAPIView.as_view(), name='stadium-list'),
    path('stadiums/create/', StadiumCreateAPIView.as_view(), name='stadium-create'),
    path('stadiums/<int:pk>/', StadiumRetrieveUpdateAPIView.as_view(), name='stadium-detail'),
    path('stadiums/<int:pk>/delete/', StadiumRetrieveDestroyAPIView.as_view(), name='stadium-delete'),

    # Order URLs
    path('orders/', OrderListAPIView.as_view(), name='order-list'),
    path('orders/create/', OrderCreateAPIView.as_view(), name='order-create'),
    path('orders/<int:pk>/', OrderRetrieveUpdateAPIView.as_view(), name='order-detail'),
    path('orders/<int:pk>/delete/', OrderRetrieveDestroyAPIView.as_view(), name='order-delete'),
]
