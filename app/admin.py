from django.contrib import admin

from app.models import UserProfile, Stadium, File, Order

admin.site.register(UserProfile)
admin.site.register(Stadium)
admin.site.register(File)
admin.site.register(Order)